from rest_framework.serializers import ModelSerializer
from subjects.models import Subject

class SubjectSerializer(ModelSerializer):
    class Meta:
        model = Subject
        fields = ['id', 'name', 'code']

class DetailSubjectSerializer(ModelSerializer):

    class Meta:
        model = Subject
        fields = '__all__'

class CreateSubjectSerializer(ModelSerializer):

    class Meta:
        model = Subject
        fields = '__all__'