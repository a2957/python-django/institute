from django.contrib import admin

# Register your models here.

from subjects.models import Subject


admin.site.register(Subject)