from django.db import models

# Create your models here.

class Subject(models.Model):

    name = models.CharField(max_length=120)
    code = models.CharField(max_length=10)
    create_at = models.DateField(auto_now_add=True)
    
    def __str__(self) -> str:
        return self.code