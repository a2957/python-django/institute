from rest_framework.viewsets import ModelViewSet
from subjects.models import Subject
from subjects.serializers import CreateSubjectSerializer, SubjectSerializer, DetailSubjectSerializer

class SubjectViewSet(ModelViewSet):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return DetailSubjectSerializer
        if self.request.method == 'POST':
            return CreateSubjectSerializer
        
        return self.serializer_class