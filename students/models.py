from django.db import models

from subjects.models import Subject

# Create your models here.

class Student(models.Model):

    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.CharField(max_length=250)
    course = models.CharField(max_length=120)
    active = models.BooleanField(default=True)
    subjects = models.ManyToManyField(Subject, related_name='students')

    def __str__(self) -> str:
        return f'{self.first_name} {self.last_name}'