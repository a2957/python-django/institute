# Generated by Django 2.2.24 on 2021-11-05 04:43

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('subjects', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200)),
                ('email', models.CharField(max_length=250)),
                ('course', models.CharField(max_length=120)),
                ('active', models.BooleanField(default=True)),
                ('subjects', models.ManyToManyField(related_name='students', to='subjects.Subject')),
            ],
        ),
    ]
