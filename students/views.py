from rest_framework.viewsets import ModelViewSet
from students.models import Student
import students.serializers as serializers


class StudentViewSet(ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = serializers.StudentSerializer

    def get_serializer_class(self):

        if self.action == 'retrieve':
            return serializers.DetailStudentSerializer

        if self.request.method == 'POST':
            return serializers.RegisterStudentSerializer

        if self.request.method == 'PUT':
            return serializers.UpdateStudentSerializer

        return serializers.StudentSerializer