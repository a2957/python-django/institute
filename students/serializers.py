from rest_framework.serializers import ModelSerializer
from subjects.serializers import SubjectSerializer
from students.models import Student

class StudentSerializer(ModelSerializer):

    class Meta:
        model = Student
        fields = ['id', 'first_name', 'last_name', 'active']


class DetailStudentSerializer(ModelSerializer):
    
    subjects = SubjectSerializer(many=True, read_only=True)

    class Meta:
        model = Student
        fields = '__all__'

class RegisterStudentSerializer(ModelSerializer):
    class Meta:
        model = Student
        fields = '__all__'

class UpdateStudentSerializer(ModelSerializer):
    class Meta:
        model = Student
        fields = ['id', 'first_name', 'last_name', 'course', 'active', 'email']